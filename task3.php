<?php
class MyCalculator{
    public $var1;
    public $var2;
    public function __construct($var1,$var2)
    {
        $this->var1=$var1;
        $this->var2=$var2;
    }

    public function add(){

        return $this->var1+$this->var2;

    }
    public function subtract(){

        return $this->var1-$this->var2;

    }
    public function multiply(){

        return $this->var1*$this->var2;

    }
    public function divide(){

        return $this->var1/$this->var2;

    }

}
$obj = new MyCalculator(12, 6);
echo "The addition is :". $obj->add()."<br>";
echo "The substract is :" .$obj->subtract()."<br>";
echo "The Multiply is :".$obj->multiply()."<br>";
echo "The Division is :".$obj->divide()."<br>";